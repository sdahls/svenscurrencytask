let app = new Vue({
	el: '#exchange',
	data() { //declaring variables used in the app
	return {
			data: "",
			exchangeData: null,
			exg: "SEK",
			selectedFromCurrency: "SEK",
			selectedToCurrency: "USD",
			inputAmount: "",
			outputAmount: "",
			currCurrency:"",
			partOne: "Part One",
			partTwo: "Part Two",
			exrate:"",
			changed: false
			
	}
},
	created(){//when the app loads, an api is called and the information is loaded into the data varialbe
			axios.get('https://api.exchangeratesapi.io/latest').then(
				resp=> (this.data=resp.data
					)
			)
		}

	,
	mounted() {
		
  	},
  	methods: {
		  //When the user clicks on buttons for different currencys, the api is called and the data is replaced with new data.
		  clicked: function(justClicked){		
			axios.get('https://api.exchangeratesapi.io/latest?base='+justClicked).then(
				resp=>(this.data=resp.data, this.exg=justClicked)
			)
		  },
		//when the user writes something in the inputfield, the value is sent to this function, the function chechs if the currency exchangerate is changed.
		//if it is, the function loads the new exchangerates, otherwise it calculates the rates with the help of the previosly loaded rates.
		  write: function(inputNum){ 
			
				if(!this.changed){
			  axios.get('https://api.exchangeratesapi.io/latest?base='+this.selectedFromCurrency).then(
				resp=>(this.exchangeData=resp.data.rates, this.exrate=this.exchangeData[this.selectedToCurrency])
			  ).then(
				() => {
					this.outputAmount = inputNum * this.exrate
					this.changed=true;
				}
			  )
			}else{
				this.outputAmount = inputNum * this.exrate
			}

		  },

		  //this funcion is called if the user want to switch the currency, the function the swiches the variables selectedfromcurrrency and selected to.
		  //After that, the function changes the input varialbe changed to false, then it calls the write function wich will get the new exchangerates
		  changePlaces: function(){
			  let tempCurrency=this.selectedFromCurrency
			  let tempCurrency2=this.selectedToCurrency
			  this.selectedFromCurrency=tempCurrency2
			  this.selectedToCurrency=tempCurrency
			  this.changed=false;
			  this.write(this.inputAmount)
		  },
		  //The hasChangeExrate is called by the selectors when they change and chanhed the changed variable to false and calls the write function wich will load the new exchangerates. 
		  hasChangeExRate: function(){
			  
			  this.changed=false;
			  this.write(this.inputAmount)
		  },
  	}
})
